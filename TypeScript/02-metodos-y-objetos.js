var Automovil = /** @class */ (function () {
    function Automovil() {
    }
    //Metodo: Algoritmo asociado a un objeto que indica la tarea que puede hacer(diferencia entre metodo y funcion es que
    //se llama metodo a las funciones de un clase u objeto en POO, se llaman funciones en la programacion estructurada)
    Automovil.prototype.mostrar = function () {
        return "Hola soy un " + this.marca + ", modelo " + this.modelo;
    };
    return Automovil;
}());
var automovil = new Automovil();
automovil.marca = "Toyota";
automovil.modelo = "2015";
console.log(automovil.mostrar());

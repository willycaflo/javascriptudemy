//var, se usa para la creacion o declaracion de variables globales
var nombre = "Juan";
if (nombre == "Juan") {
    //let, se usa para crar variables locales, dentro de bloques de codigo(function, condicional, ciclo)
    var nombre_1 = "Pedro";
    console.log("Dentro de la condicion nombre: ", nombre_1);
}
console.log("Fuera de la condicion nombre: ", nombre);
